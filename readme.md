# README : Magic Cube

## Introduction :

Magic cube vous plonge d'une application RV où vous pouvez librement manipuler un cube de type "Rubik's". Ce cube est tout à fait classique, de taille 3x3, est manipulable à l'aide d'un LeapMotion SDK.

## Manipulation :

Les faces du cube tournent via des mouvements des mains droites et gauches de l'utilisateur : la main droite fait apparemment tourner les faces plus à droite de l'utilisateur et inversement.

Il est possible de choisir quel face faire tourner en fonction du nombre de doigts utilisés pour indiquer un mouvement : 1, 2 ou 3 doigts pour respectivement gauche/droite, bas/gaut et avant/arrière.

Afin de pouvoir changer les faces que l'on regardait, il nous a fallu mettre en place un élément visuel permettant de faire tourner le cube sur lui-même. Cela est réalisé au moyen d'une sphère à gauche de l'utilisateur. La manipuler revient à manipuler la rotation du cube sur lui-même.

## Scénario type :

Au lancement de l'application, l'utilisateur est face à un cube correctement agencé.

Il est alors libre d'effectuer toutes les rotations de celui-ci comme il le désire, pour le désordonner.

Quand il le souhaite, il se met alors à tenter de résoudre le problème, et doit ainsi essayer d'uniformiser les couleurs des faces.

Il s'exerce ainsi jusqu'à en avoir assez, et peut alors quitter l'application.
